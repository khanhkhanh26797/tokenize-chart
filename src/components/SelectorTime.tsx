import React from 'react';

import './styles.css';
// const api = require('@marcius-capital/binance-api')
import 'bootstrap/dist/css/bootstrap.min.css';
import useAppState from './useAppState'
import { dataTimeShow } from './AppStateContext';

const SelectorTime = ({ openModalCallback }:{
  openModalCallback : () => void
}) => {
  //@ts-ignore
  const { timeSlot, setTimeSlotGlobal, timeData } = useAppState();

  const openModal = () => {
    openModalCallback()
  }

  const chooseTime = (timeSlot:dataTimeShow) => {
    setTimeSlotGlobal(timeSlot.value)
  }

  return (
    <div className="selectorContainer">
      {timeData.map((item, index) => {
        return <>{item.checked ? <div key = {index} style={{ color: item.value === timeSlot && item.title !== 'Time' ? "red" : "#f0b90b" }} onClick={() => chooseTime(item)} className="selectorTime">{item.title} </div> : null} </>
      })}
      <button type="button" className="button" onClick={openModal} >
        <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M0.300049 0.5L5.30005 5.5L10.3 0.5H0.300049Z" fill="#ffffff" />
        </svg>
      </button>
    </div>
  );
}

export default SelectorTime;
