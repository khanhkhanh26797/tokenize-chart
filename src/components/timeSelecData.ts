export const timeSelecData = [
  {
    title: 'Time',
    value: '1m',
    checked: true,
  },
  {
    title: '1m',
    value: '1m',
    checked: true,
  },
  {
    title: '3m',
    value: '3m',
    checked: true,
  },
  {
    title: '5m',
    value: '5m',
    checked: true,
  },
  {
    title: '15m',
    value: '15m',
    checked: true,
  },
  {
    title: '30m',
    value: '30m',
    checked: true,
  },
  {
    title: '1H',
    value: '1h',
    checked: true,
  },
  {
    title: '2H',
    value: '2h',
    checked: true,
  },
  {
    title: '4H',
    value: '4h',
    checked: true,
  },
  {
    title: '6H',
    value: '6h',
    checked: true,
  },
  {
    title: '8H',
    value: '8h',
    checked: true,
  },
  {
    title: '12H',
    value: '12h',
    checked: true,
  },
  {
    title: '1D',
    value: '1d',
    checked: true,
  },
  {
    title: '3D',
    value: '3d',
    checked: true,
  },
  {
    title: '1W',
    value: '1w',
    checked: true,
  },
  {
    title: '1M',
    value: '1M',
    checked: true,
  }
]