import React, { useState } from 'react';

import './styles.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import useAppState from './useAppState'
import { dataTimeShow } from './AppStateContext';

const ModalSelectorTime = ({ closeModalCallback, onChooseTimeModalCallBack }: {
  closeModalCallback: () => void
  onChooseTimeModalCallBack: (value: string) => void
}): JSX.Element => {
  const [editing, setEditing] = useState<boolean>(false);
  const { timeData, setTimeDataGlobal } = useAppState();

  const onEditAndSave = () => {
    setEditing(!editing);
    if (editing) {
      closeModalCallback()
    }
  }

  const onCheckItem = (item:dataTimeShow) => {
    if (editing) {
      timeData.map((timeSlot) => {
        if (timeSlot.value === item.value) {
          timeSlot.checked = !timeSlot.checked
        }
        return null;
      })
      setTimeDataGlobal([...timeData])
    }
    else {
      // eslint-disable-next-line array-callback-return
      timeData.some((element, index) => {
        if (element.title === item.title && element.checked === false) {
          timeData[index].checked = true;
          return true;
        }
      })
      setTimeDataGlobal([...timeData])
      onChooseTimeModalCallBack(item.value)
    }
  }

  return (
    <div className="modalOverLayer">
      <div className="modalSticky">
        <div className="modalContainer">
          <div className="modalHeader">
            <div className="modalHeaderText">Select interval</div>
            <button className="button" onClick={onEditAndSave} >
              {editing ? 'Save' : 'Edit'}
            </button>
          </div><div className="menuOptionContainer">
            {timeData.map((item) => {
              return (
                <>
                  <div className="itemContainer">
                    <div className="itemOption" style={{ backgroundColor: item.checked ? '#f0b90b4d' : 'gray' }} onClick={() => onCheckItem(item)}>{item.title}</div>
                    {editing ? <div className="itemIcon">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none">
                        <path d="M20.5 7.42L9.41 18.5 8 17.09l-5-5 1.41-1.42 5 5L19.08 6l1.42 1.42z" fill={item.checked ? '#f0b90b4d' : 'black'}></path>
                      </svg>
                    </div> : null}
                  </div>
                </>
              )
            })
            }
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModalSelectorTime;
