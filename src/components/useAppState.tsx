import { AppStateContext } from './AppStateContext';
import { useContext } from 'react';

export default () => useContext(AppStateContext);
