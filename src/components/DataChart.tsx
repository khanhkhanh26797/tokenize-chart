import React, { useEffect, useState } from 'react';
import { CrosshairMode } from 'lightweight-charts';
import { converAPItoDataChart } from '../mapperData'
import Chart from 'kaktana-react-lightweight-charts'
import './styles.css';
import useWindowDimensions from '../useWindowDimensions'
import useAppState from './useAppState'

const api = require('@marcius-capital/binance-api')
const OtherChart = () => {
  const { timeSlot } = useAppState();

  const { height, width } = useWindowDimensions();
  const options = ({
    height: height ? height - 50 : 1000,
    width: width,
    layout: {
      backgroundColor: '#253248',
      textColor: 'rgba(255, 255, 255, 0.9)',
    },
    grid: {
      vertLines: {
        color: '#334158',
      },
      horzLines: {
        color: '#334158',
      },
    },
    crosshair: {
      mode: CrosshairMode.Normal,
    },
    priceScale: {
      borderColor: '#485c7b',
    },
    timeScale: {
      timeVisible: true,
      secondsVisible: false,
    },
  })
  const [candlestickSeries, setCandlestickSeries] = useState<any>([{ data: [] }])
  useEffect(() => {
    api.rest.klines({ symbol: 'BTCUSDT', interval: timeSlot, limit: 500 }).then((res: any[]) => {
      console.log('candleSeries', res)
      const dataAPI = [{
        data: converAPItoDataChart(res)
      }]
      setCandlestickSeries(dataAPI);
    })
  }, [timeSlot]);
  return (
    <div className="App">
      <Chart options={options} candlestickSeries={candlestickSeries} autoWidth height={320} />
    </div>
  )
}

export default OtherChart;

