import React, {
  ReactNode,
  createContext,
  memo,
  useCallback,
  useMemo,
  useState,
} from 'react';
import { timeSelecData } from './timeSelecData'

export type dataTimeShow = {
  title: string,
  value: string,
  checked: boolean,
}
type AppStateProviderProps = {
  children: ReactNode;
};

export const AppStateContext = createContext({
  timeSlot: '1m',
  timeData: timeSelecData,
  setTimeSlotGlobal: (timeSlot: string) => {
    return;
  },
  setTimeDataGlobal: (timeData: Array<dataTimeShow>) => {
    return;
  },
});

const AppStateProvider = ({ children }: AppStateProviderProps) => {
  const [timeSlot, setTimeSlot] = useState<string>(
    '1m'
  );
  const [timeData, setTimeData] = useState<Array<dataTimeShow>>(timeSelecData);

  const setTimeSlotGlobal: (timeSlot: string) => void = useCallback((timeSlot: string) => {
    console.log(
      'setTimeSlotGlobal',
      timeSlot,
    );
    setTimeSlot(timeSlot)
  }, []);

  const setTimeDataGlobal: (timeData: Array<dataTimeShow>) => void = useCallback((timeData: Array<dataTimeShow>) => {
    console.log(
      'setTimeDataGlobal',
      timeData,
    );
    setTimeData(timeData)
  }, []);

  const value = useMemo(
    () => ({
      timeSlot: timeSlot,
      timeData: timeData,
      setTimeSlotGlobal,
      setTimeDataGlobal
    }),
    [setTimeDataGlobal, setTimeSlotGlobal, timeData, timeSlot],
  );

  return (
    <AppStateContext.Provider value={value}>
      {children}
    </AppStateContext.Provider>
  );
};

export default memo(AppStateProvider);
