import React, { useState, useCallback } from 'react';

import './styles.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import SelectorTime from './SelectorTime'
import ModalSelectorTime from './ModalSelectorTime'
import useAppState from './useAppState'
const SelectorBar = () => {
  const [showModal, setShowModal] = useState(false);

  const { setTimeSlotGlobal } = useAppState();

  const openModal = useCallback(() => {
    setShowModal(!showModal);
  }, [showModal]);

  const onTimeSlotModalChange = useCallback((itemSlot:string) =>{
    setShowModal(false);
    setTimeSlotGlobal(itemSlot)
  },[setTimeSlotGlobal])
  
  const closeModal = useCallback(() => {
    setShowModal(false);
  }, []);


  return (
    <>
      <SelectorTime openModalCallback={openModal} />
      {showModal ? <ModalSelectorTime closeModalCallback={closeModal} onChooseTimeModalCallBack={onTimeSlotModalChange} /> : null}
    </>
  );
}

export default SelectorBar;
