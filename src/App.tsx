import React from 'react';
import './App.css';
import SelectorBar from './components/SelectorBar'
import DataChart from './components/DataChart'

const App = () => {

  return (
    <div className="App">
      <SelectorBar />
      <DataChart/>
    </div>
  );
}

export default App;

